<?php

namespace App\Dev\Swift;

use Swift_Events_SendEvent as SendEvent;

class LoggerMailsPlugin implements \Swift_Events_SendListener
{
    private $fileName;

    private $splFile = null;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function beforeSendPerformed(SendEvent $event)
    {
        $this->printToFile($event, 'beforeSend');
    }

    public function sendPerformed(SendEvent $event)
    {
        $this->printToFile($event, 'send');
    }

    private function printToFile(SendEvent $event, string $eventName)
    {
        $message = $event->getMessage();

        $data = sprintf(
            "[%s] %s from: %s, cc: (%s), bcc: (%s), msg: %s\n",
            date('Y-m-d H:i:s'),
            $eventName,
            $message->getFrom(),
            $this->dumpEmailsAsString($message->getCc()),
            $this->dumpEmailsAsString($message->getBcc()),
            $this->getShortMsg($message->getBody())
        );

        $this->getSplFile()->fwrite($data);
    }

    private function getShortMsg(string $msg): string
    {
        $text = trim($msg);
        $text = preg_replace('/\s{1,}/', " ", $text);
        $text = mb_substr($text, 0, 10);
        $text = str_replace(' ', '·', $text);

        return $text;
    }

    private function dumpEmailsAsString(array $emails): string
    {
        return $emails ? implode(', ', $emails) : 'empty';
    }

    private function getSplFile(): \SplFileObject
    {
        if (null === $this->splFile) {
            touch($this->splFile);
            $this->splFile = new \SplFileObject($this->fileName, 'a+');
        }

        return $this->splFile;
    }
}
