<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    /**
     * @Route("/{page}", name="home", requirements={"page" = "[1-9][0-9]*"}, defaults={"page" = 1})
     *
     * @param $request    Request
     * @param $repository ProductRepository
     *
     * @return Response
     */
    public function index(Request $request, ProductRepository $repository)
    {
        $products = $repository->getPagination($request->attributes->getInt('page', 1));

        return $this->render('home/index.html.twig', array('products' => $products));
    }
}
