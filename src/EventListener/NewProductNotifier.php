<?php

namespace App\EventListener;

use App\Entity\Product;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class NewProductNotifier
{
    private $mailer;

    private $noreplyEmail;

    private $loggerEmail;

    public function __construct(\Swift_Mailer $mailer, string $noreplyEmail, string $loggerEmail)
    {
        $this->mailer = $mailer;
        $this->noreplyEmail = $noreplyEmail;
        $this->loggerEmail = $loggerEmail;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();
        if ($object instanceof Product) {
            $this->notify($object);
        }
    }

    private function notify(Product $product)
    {
        $message = (new \Swift_Message('New product has been added'))
            ->setFrom($this->noreplyEmail)
            ->setTo($this->loggerEmail)
            ->setBody(
                sprintf('Added new product [%d] %s', $product->getId(), $product->getName()),
                'text/plain'
            );

        $this->mailer->send($message);
    }
}
