<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository
{
    private $paginator;

    private $limit;

    public function __construct(RegistryInterface $registry, PaginatorInterface $paginator, int $limitPerPage)
    {
        parent::__construct($registry, Product::class);
        $this->paginator = $paginator;
        $this->limit = $limitPerPage;
    }

    public function getPagination(int $page)
    {
        $query = $this->createQueryBuilder('p')
            ->orderBy('p.id', 'ASC')
            ->getQuery();

        return $this->paginator->paginate($query, $page, $this->limit);
    }
}
