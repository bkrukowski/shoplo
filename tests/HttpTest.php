<?php

namespace App\Tests;

use Symfony\Component\HttpFoundation\Response;

class HttpTest extends TestBase
{
    /**
     * @dataProvider providerPage
     *
     * @param string $method
     * @param string $path
     */
    public function testPage(string $method, string $path)
    {
        $tag = static::bootKernel()->getContainer()->getParameter('verificationTag');
        $client = static::createClient();
        $client->request($method, $path);

        $this->assertNotSame('', $tag, 'Verification tag cannot be empty');
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertContains($tag, $client->getResponse()->getContent());
    }

    public function providerPage()
    {
        yield array('GET', '/');
        yield array('GET', '/1');
    }

    /**
     * @dataProvider providerStatus
     *
     * @param string $method
     * @param string $path
     * @param int    $status
     */
    public function testStatus(string $method, string $path, int $status)
    {
        $tag = static::bootKernel()->getContainer()->getParameter('verificationTag');
        $client = static::createClient();
        $client->request($method, $path);

        $this->assertEquals($status, $client->getResponse()->getStatusCode());
    }

    public function providerStatus()
    {
        yield array('GET', '/', Response::HTTP_OK);
        yield array('GET', '/2', Response::HTTP_OK);
        yield array('PUT', '/', Response::HTTP_OK);
        yield array('GET', '/not-found', Response::HTTP_NOT_FOUND);
        yield array('GET', '/admin/new-product', Response::HTTP_UNAUTHORIZED);
    }
}
