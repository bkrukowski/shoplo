<?php

namespace App\Tests\EventListener;

use App\Entity\Product;
use App\Tests\TestBase;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class NewProductNotifierTest extends TestBase
{
    public function testNotification()
    {
        $mailerId = 'swiftmailer.mailer.default';
        $client = static::bootKernel();
        $container = $client->getContainer();

        /** @var \Swift_Mailer $originalMailer */
        $mailer = $this->getMockMailer();
        $container->set($mailerId, $mailer);

        /** @var ManagerRegistry $doctrine */
        $doctrine = $container->get('doctrine');
        $em = $doctrine->getManager();

        $product = new Product();
        $product->setPrice(6000);
        $product->setName('iPhone');
        $product->setDescription(implode(' ', array_fill(0, 100, 'lorem ipsum')));

        $em->persist($product);
        $this->assertSame(0, $mailer->getTicks());
        $em->flush();
        $this->assertSame(1, $mailer->getTicks());
    }

    private function getMockMailer()
    {
        return new class() extends \Swift_Mailer {
            private $ticks = 0;

            public function __construct()
            {
            }

            public function getTicks(): int
            {
                return $this->ticks;
            }

            public function send(\Swift_Mime_SimpleMessage $message, &$failedRecipients = null)
            {
                ++$this->ticks;
            }
        };
    }
}
