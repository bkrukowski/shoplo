<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class TestBase extends WebTestCase
{
    protected function setUp()
    {
        $this->expectOutputString('');
    }
}
